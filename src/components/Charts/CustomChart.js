import {Line} from 'vue-chartjs'
import reactiveProp from './mixin.js'

export default Line.extend({
  mixins: [reactiveProp],
  props: ['chartData', 'options'],
  mounted () {
    this.renderChart(this.chartData, this.options)
  }
})